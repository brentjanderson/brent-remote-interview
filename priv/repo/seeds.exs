# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     BrentRemote.Repo.insert!(%BrentRemote.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

BrentRemote.Repo.query!("TRUNCATE users")

now = NaiveDateTime.truncate(NaiveDateTime.utc_now(), :second)

users =
  for _ <- 1..1_000_000 do
    %{
      id: UUID.uuid4(),
      points: 0,
      inserted_at: now,
      updated_at: now
    }
  end

# Postgres limits terms per SQL to 65,535
# Each batch size is that constant divided by the number of fields in each record
pg_max_terms = 65_535
chunk_size = (pg_max_terms / (hd(users) |> Map.keys() |> length())) |> floor()

Enum.chunk_every(users, chunk_size)
|> Enum.each(fn chunk ->
  BrentRemote.Repo.insert_all(BrentRemote.Users.User, chunk)
end)
