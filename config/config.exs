# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :brent_remote,
  ecto_repos: [BrentRemote.Repo],
  generators: [binary_id: true]

# Configures the endpoint
config :brent_remote, BrentRemoteWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "bhk8ZhNZwNkblZY+wB5Zx1gDBpVMvA+hZaX8bnrUGj0RALb4OwgLwkAP+hNDauX5",
  render_errors: [view: BrentRemoteWeb.ErrorView, accepts: ~w(json), layout: false],
  pubsub_server: BrentRemote.PubSub,
  live_view: [signing_salt: "jxrnwDXA"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
