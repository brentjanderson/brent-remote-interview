defmodule BrentRemoteWeb.UserController do
  @moduledoc """
  Controller for handling interaction with users
  """

  use BrentRemoteWeb, :controller

  action_fallback BrentRemoteWeb.FallbackController

  @doc """
  Retrieve two users from our PointsReadServer along with the timestamp of the last query
  """
  def index(conn, _params) do
    %{last_query_timestamp: ts, users: users} = BrentRemote.PointsReadServer.fetch_users()

    json(conn, %{
      users: users,
      timestamp: ts
    })
  end
end
