defmodule BrentRemote.PointsWriteServer do
  @moduledoc """
  Service for updating user points

  This is split from BrentRemote.PointsReadServer to ensure that reads are never blocked by updates.
  """

  use GenServer

  alias BrentRemote.Users

  @point_update_interval 60_000

  def start_link(_) do
    GenServer.start_link(
      __MODULE__,
      nil,
      name: __MODULE__
    )
  end

  @impl true
  def init(_) do
    # Update points 30 seconds after starting to avoid a race condition with
    # the read server
    schedule_update_points(30_000)
    {:ok, nil}
  end

  @impl true
  @doc """
  Randomize user points and schedule the next time we will randomize points.
  """
  def handle_info(:update_points, state) do
    :ok = Users.randomize_all_points()
    schedule_update_points()

    {:noreply, state}
  end

  defp schedule_update_points(delay \\ @point_update_interval) do
    Process.send_after(self(), :update_points, delay)
  end
end
