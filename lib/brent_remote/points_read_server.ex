defmodule BrentRemote.PointsReadServer do
  @moduledoc """
  Service for reading user points.

  Paired with BrentRemote.PointsWriteServer to ensure we have non-blocking IO
  for reads and writes.
  """

  use GenServer

  alias BrentRemote.Users

  @max_number_update_interval 60_000

  @doc """
  Retrieve two users with maximum points and the time of the most recent query
  """
  def fetch_users(timeout \\ 5000), do: GenServer.call(__MODULE__, :fetch_users, timeout)

  def start_link(_) do
    GenServer.start_link(
      __MODULE__,
      %{max_number: random_range()},
      name: __MODULE__
    )
  end

  @impl true
  def init(%{max_number: max_number} \\ %{max_number: random_range()}) do
    schedule_update_max_number()
    {:ok, %{max_number: max_number, last_query_timestamp: nil, users_cache: []}}
  end

  @impl true
  @doc """
  Handle delayed callback to update the threshold for filtering users.
  """
  def handle_info(:update_max_number, %{users_cache: users_cache} = state) do
    max_number = random_range()

    users =
      try do
        Users.get_top_users(max_number)
      rescue
        DBConnection.ConnectionError -> users_cache
      end

    schedule_update_max_number()

    {:noreply, %{state | max_number: max_number, users_cache: users}}
  end

  @impl true
  @doc """
  Retrieve the users and the most recent query timestamp.

  If the database is down, use the most users retrieved so that we degrade service instead of completely failing.
  """
  def handle_call(
        :fetch_users,
        _from,
        %{
          users_cache: users,
          last_query_timestamp: last_query_timestamp
        } = state
      ) do
    now = DateTime.utc_now()
    formatted_timestamp = format_timestamp(last_query_timestamp)

    {:reply, %{last_query_timestamp: formatted_timestamp, users: users},
     %{state | last_query_timestamp: now, users_cache: users}}
  end

  defp random_range(low \\ 0, high \\ 100) do
    Range.new(low, high)
    |> Enum.random()
  end

  defp schedule_update_max_number(delay \\ @max_number_update_interval) do
    Process.send_after(self(), :update_max_number, delay)
  end

  defp format_timestamp(nil), do: nil
  defp format_timestamp(ts), do: Calendar.strftime(ts, "%Y-%m-%d %H:%M:%S")
end
