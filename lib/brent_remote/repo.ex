defmodule BrentRemote.Repo do
  use Ecto.Repo,
    otp_app: :brent_remote,
    adapter: Ecto.Adapters.Postgres
end
