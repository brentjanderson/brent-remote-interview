defmodule BrentRemote.Users do
  @moduledoc """
  The Users context.
  """

  import Ecto.Query, warn: false
  alias BrentRemote.Repo

  alias BrentRemote.Users.User

  @doc """
  Gets a single user.

  Raises `Ecto.NoResultsError` if the User does not exist.

  ## Examples

      iex> get_user!(123)
      %User{}

      iex> get_user!(456)
      ** (Ecto.NoResultsError)

  """
  def get_user!(id), do: Repo.get!(User, id)

  @doc """
  Creates a user.

  ## Examples

      iex> create_user(%{field: value})
      {:ok, %User{}}

      iex> create_user(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_user(attrs \\ %{}) do
    %User{}
    |> User.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a user.

  ## Examples

      iex> update_user(user, %{field: new_value})
      {:ok, %User{}}

      iex> update_user(user, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_user(%User{} = user, attrs) do
    user
    |> User.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Update all users to have random points set

  Use native SQL to optimize performance

  ## Examples

    iex> randomize_all_points()
    :ok

  """
  def randomize_all_points do
    Repo.query!("UPDATE users SET points = floor(random() * (100 + 1))::int, updated_at = NOW()")

    :ok
  end

  @doc """
  Get up to two users who have at least max_number points

  ## Examples

    iex> get_top_users(0)
    [%User{}, %User{}]

    iex> get_top_users(101)
    []

  """
  def get_top_users(max_number) do
    query = from u in User, limit: 2, where: u.points >= ^max_number
    Repo.all(query)
  end
end
