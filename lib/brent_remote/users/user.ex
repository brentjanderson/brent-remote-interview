defmodule BrentRemote.Users.User do
  @moduledoc """
  Ecto schema for our users model
  """

  @derive {Jason.Encoder, only: [:id, :points]}

  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "users" do
    field :points, :integer

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:points])
    |> validate_required([:points])
    |> validate_number(:points,
      greater_than_or_equal_to: 0,
      less_than_or_equal_to: 100,
      message: "must be between 0 and 100"
    )
  end
end
