# Brent Remote interview exercise

This was fun. Here's how to boot it up:

```shell
# If you have docker...
docker-compose up -d
# If you don't have docker, you just need postgres running on port 5432 on your box

mix deps.get

mix ecto.setup

# Boot up the app
mix phx.server

# Visit http://localhost:4000 to query the app

# The tests work too - with coverage!
mix test --cover

# So does Credo, Sobelow, and Dialyzer
mix credo --strict
mix sobelow --config
mix dialyzer
```

## Second draft edits

Based on feedback from the interview process, I made a few changes:

1. I split `PointServer` into two pieces to avoid blocking reads or writes when running with a million users:
   1. `PointsReadServer` returns the top two users. It also has an internal timer for updating the `max_number` every minute.
      - `PointsReadServer.fetch_users/1` also keeps a cache of the most recently returned two users. It refreshes this cache every time we update `max_number`. This ensures that page loads are very fast (~1ms vs ~50ms on my local box), it keeps database load low, and if the database is down for some reason we still return the most recent response.
   2. `PointsWriteServer` has a timer for calling `Users.randomize_all_points/0` every minute.
   3. Both GenServers are fully tested, primarily by calling the `handle_*` methods directly for speedy tests, and also by invoking the running GenServer as needed to ensure full integration test coverage.
2. I added tests for:
   1. The context methods `Users.randomize_all_points/0` and `Users.get_top_users/1`.
   2. Both `PointsReadServer` and `PointsWriteServer`. They have 92.3% and 100% test coverage, respectively.
      - The missing test coverage in `PointsReadServer` is because I couldn't figure out how to test when the database is down, apart from manually shutting down the database. I know it works from manual testing, but I didn't figure out how to break Postgrex by hand in a test case.
3. I removed unused generated code from the User context & test files.
4. I added a return value on `Users.randomize_all_points/0` to make it clear that the operation succeeded.
5. I added the alias `mix quality` to run static security & code quality tests (credo, sobelow, dialyzer).
6. I added test coverage checking with Coveralls to ensure adequate test coverage in the project.
7. I also added `mix test.watch` to make it easier to run through TDD cycles against the project.
8. I set up Gitlab CI to verify the above on the repository.

Although I would still love the opportunity to work at Remote, I am grateful for the feedback on the code challenge as it has helped me improve how I approach testing OTP applications. If throwing a proper LiveView frontend on this app, or building a Nerves project to visualize how many countries Remote is in would help you reconsider my application, let me know.

> We are not afraid to fail and make mistakes. Fail fast, learn fast!
> ~ From [the Remote Handbook](https://www.notion.so/Values-d43475e8895c4b7c811490aef64782b3#a0d7538acb8447e1994d1dbfd6facc12) under "Ambition > Continuous Growth"
