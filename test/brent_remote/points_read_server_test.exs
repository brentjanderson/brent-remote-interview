defmodule BrentRemote.PointsReadServerTest do
  use BrentRemote.DataCase, async: true

  alias BrentRemote.PointsReadServer
  alias BrentRemote.Users

  describe "points_read_server" do
    @valid_attrs %{points: 42}

    def user_fixture(attrs \\ %{}) do
      {:ok, user} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Users.create_user()

      user
    end

    test "fetch_users/0 returns no users when there are no users" do
      state = %{max_number: 0, last_query_timestamp: nil, users_cache: []}

      assert {:reply, %{last_query_timestamp: nil, users: []},
              %{last_query_timestamp: _, max_number: 0}} =
               PointsReadServer.handle_call(:fetch_users, self(), state)
    end

    test "fetch_users/0 returns not nil for the last_query_timestamp after the first call" do
      state = %{max_number: 0, last_query_timestamp: nil, users_cache: []}

      assert {:reply, %{last_query_timestamp: nil, users: []}, new_state} =
               PointsReadServer.handle_call(:fetch_users, self(), state)

      assert {:reply, %{last_query_timestamp: _, users: []},
              %{last_query_timestamp: ts, max_number: 0}} =
               PointsReadServer.handle_call(:fetch_users, self(), new_state)

      assert ts != nil
    end

    test "fetch_users/0 returns no users when points > max_points of any user" do
      user_fixture(%{points: 0})
      user_fixture(%{points: 0})
      user_fixture(%{points: 0})

      state = %{max_number: 1, last_query_timestamp: nil, users_cache: []}

      assert {:reply, %{last_query_timestamp: nil, users: []},
              %{last_query_timestamp: _, max_number: 1}} =
               PointsReadServer.handle_call(:fetch_users, self(), state)
    end

    test "fetch_users/0 returns at most two users when points > max_points of those users" do
      user_a = user_fixture(%{points: 5})
      user_b = user_fixture(%{points: 10})
      _user_c = user_fixture(%{points: 15})

      state = %{max_number: 1, last_query_timestamp: nil, users_cache: [user_a, user_b]}

      assert {:reply, %{last_query_timestamp: nil, users: [^user_a, ^user_b]},
              %{last_query_timestamp: _, max_number: 1}} =
               PointsReadServer.handle_call(:fetch_users, self(), state)
    end

    test ":update_max_number callback randomizes the max_points used when querying" do
      state = %{users_cache: [], max_number: nil, last_query_timestamp: nil}

      # Ensure test is deterministic
      :rand.seed(:exrop, {101, 102, 103})

      assert {:noreply, %{max_number: max_number}} =
               PointsReadServer.handle_info(:update_max_number, state)

      assert max_number == 28
    end

    test ":update_max_number callback updates the users cache with the most recent users" do
      _user_a = user_fixture(%{points: 20})
      user_b = user_fixture(%{points: 30})
      user_c = user_fixture(%{points: 40})

      state = %{max_number: nil, users_cache: [], last_query_timestamp: nil}

      # Ensure test is deterministic
      :rand.seed(:exrop, {101, 102, 103})

      assert {:noreply,
              %{
                last_query_timestamp: nil,
                users_cache: [^user_b, ^user_c]
              }} = PointsReadServer.handle_info(:update_max_number, state)
    end
  end
end
