defmodule BrentRemote.UsersTest do
  use BrentRemote.DataCase, async: true

  alias BrentRemote.Users

  describe "users" do
    alias BrentRemote.Users.User

    @valid_attrs %{points: 42}
    @update_attrs %{points: 43}
    @invalid_attrs %{points: nil}

    def user_fixture(attrs \\ %{}) do
      {:ok, user} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Users.create_user()

      user
    end

    test "randomize_all_points/0 randomizes points in the database" do
      # Ensure test is deterministic
      BrentRemote.Repo.query!("SELECT setseed(0.5)")
      user = user_fixture()
      :ok = Users.randomize_all_points()
      updated_user = Users.get_user!(user.id)
      assert updated_user.points != user.points
    end

    test "get_top_users/1 returns at most two records" do
      [_user_a, _user_b, _user_c] = [user_fixture(), user_fixture(), user_fixture()]
      fetched_users = Users.get_top_users(0)
      assert length(fetched_users) == 2
    end

    test "get_top_users/1 returns the users with at least max_points" do
      [user_a, user_b, _user_c] = [user_fixture(), user_fixture(), user_fixture()]

      {:ok, user_a} = Users.update_user(user_a, %{points: 50})
      fetched_users = Users.get_top_users(49)
      assert fetched_users == [user_a]

      {:ok, user_b} = Users.update_user(user_b, %{points: 50})
      fetched_users = Users.get_top_users(49)
      assert fetched_users == [user_a, user_b]

      fetched_users = Users.get_top_users(99)
      assert fetched_users == []
    end

    test "get_user!/1 returns the user with given id" do
      user = user_fixture()
      assert Users.get_user!(user.id) == user
    end

    test "create_user/1 with valid data creates a user" do
      assert {:ok, %User{} = user} = Users.create_user(@valid_attrs)
      assert user.points == 42
    end

    test "create_user/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Users.create_user(@invalid_attrs)
    end

    test "create_user/1 requires points be between 0 - 100" do
      assert {:error, %Ecto.Changeset{}} = Users.create_user(%{points: -1})
      assert {:error, %Ecto.Changeset{}} = Users.create_user(%{points: 101})
    end

    test "update_user/2 with valid data updates the user" do
      user = user_fixture()
      assert {:ok, %User{} = user} = Users.update_user(user, @update_attrs)
      assert user.points == 43
    end

    test "update_user/2 with invalid data returns error changeset" do
      user = user_fixture()
      assert {:error, %Ecto.Changeset{}} = Users.update_user(user, @invalid_attrs)
      assert user == Users.get_user!(user.id)
    end
  end
end
