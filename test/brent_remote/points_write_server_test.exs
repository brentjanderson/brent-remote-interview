defmodule BrentRemote.PointsWriteServerTest do
  use BrentRemote.DataCase, async: true

  alias BrentRemote.PointsWriteServer
  alias BrentRemote.Users

  describe "points_write_server" do
    @valid_attrs %{points: 42}

    def user_fixture(attrs \\ %{}) do
      {:ok, user} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Users.create_user()

      user
    end

    test "update_points/0 randomizes the points of the users in the database" do
      user_a = user_fixture(%{points: 5})
      user_b = user_fixture(%{points: 10})
      user_c = user_fixture(%{points: 15})

      # Ensure test is deterministic
      BrentRemote.Repo.query!("SELECT setseed(0.5)")

      assert {:noreply, _} = PointsWriteServer.handle_info(:update_points, nil)

      assert Users.get_user!(user_a.id).points == 25
      assert Users.get_user!(user_b.id).points == 52
      assert Users.get_user!(user_c.id).points == 46
    end
  end
end
