defmodule BrentRemoteWeb.UserControllerTest do
  use BrentRemoteWeb.ConnCase

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all users", %{conn: conn} do
      conn = get(conn, Routes.user_path(conn, :index))
      assert %{"timestamp" => _, "users" => []} = json_response(conn, 200)
    end
  end
end
